let collection = [];
// Write the queue functions below. 


//Gives all the elements of the queue
function print(){
    return(collection)
}

//Adds an element/elements to the end of the queue
function enqueue(item){
    collection.push(item)

}

//Removes an element in front of the queue
function dequeue(){
   if(collection.length > 0){
    return collection.shift();
   }
}


//Shows the element at the front
function front(){
    return collection[0]
}

//Shows the total number of elements
function size(){
    return collection.length;
}

//Gives a Boolean value describing whether the queue is empty or not
function isEmpty(){
    if (isEmpty === ""){
        return true;
    }else{
        return false;
    }
}







// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
